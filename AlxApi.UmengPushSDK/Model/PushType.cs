﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlxApi.UmengPushSDK.Model
{
    /// <summary>
    /// 推送类型
    /// </summary>
    public class PushType
    {
        /// <summary>
        /// 单播
        /// </summary>
        public static string Unicast = "unicast";
        /// <summary>
        /// 列播(要求不超过500个device_token)
        /// </summary>
        public static string Listcast = "listcast";
        /// <summary>
        /// 文件播  (多个device_token可通过文件形式批量发送）
        /// </summary>
        public static string Filecast = "filecast";
        /// <summary>
        /// 广播
        /// </summary>
        public static string Broadcast = "broadcast";
        /// <summary>
        /// 组播(按照filter条件筛选特定用户群, 具体请参照filter参数)
        /// </summary>
        public static string Groupcast = "groupcast";
        /// <summary>
        /// (通过开发者自有的alias进行推送), 包括以下两种case:- alias: 对单个或者多个alias进行推送- file_id: 将alias存放到文件后，根据file_id来推送
        /// </summary>
        public static string Customizedcast = "customizedcast";
    }
}

