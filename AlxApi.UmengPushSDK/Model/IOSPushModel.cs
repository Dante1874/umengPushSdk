﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlxApi.UmengPushSDK.Model
{
    /// <summary>
    /// ios推送实体
    /// </summary>
    public class IOSPushModel : PushBaseModel
    {
        /// <summary>
        /// 必填 消息内容(iOS最大为2012B), 包含参数说明如下(JSON格式):
        /// </summary>
        public payloadInfo payload { get; set; }
        /// <summary>
        /// 必填 消息内容(iOS最大为2012B), 包含参数说明如下(JSON格式):
        /// </summary>
        public class payloadInfo
        {   /// <summary>
            /// // 必填 严格按照APNs定义来填写
            /// </summary>
            public apsInfo aps { get; set; }
            /// <summary>
            /// // 必填 严格按照APNs定义来填写
            /// </summary>
            public class apsInfo
            {
                /// <summary>
                ///  必填    
                /// </summary>
                public string alert { get; set; }
                /// <summary>
                /// 可选
                /// </summary>
                [ScriptIgnore]
                public string badge { get; set; }
                /// <summary>
                /// 可选
                /// </summary>
                [ScriptIgnore]
                public string sound { get; set; }
                /// <summary>
                /// 可选
                /// </summary>
                [ScriptIgnore]
                public string content_available { get; set; }
            }
        }
        /// <summary>
        /// 可选 发送策略
        /// </summary>
        [ScriptIgnore]
        public policyInfo policy { get; set; }
        /// <summary>
        /// 可选 发送策略
        /// </summary> 
        public class policyInfo
        {
            /// <summary>
            /// 可选 定时发送时间，默认为立即发送。发送时间不能小于当前时间。 格式: "YYYY-MM-DD HH:mm:ss"。 
            /// </summary>
            [ScriptIgnore]
            public string start_time { get; set; }
            /// <summary>
            /// 可选 消息过期时间,其值不可小于发送时间, 默认为3天后过期。格式同start_time
            /// </summary>
            [ScriptIgnore]
            public string expire_time { get; set; }
            /// <summary>
            /// 可选 发送限速，每秒发送的最大条数。开发者发送的消息如果有请求自己服务器的资源，可以考虑此参数。
            /// </summary>
            [ScriptIgnore]
            public string max_send_num { get; set; }


        }

    }
}
