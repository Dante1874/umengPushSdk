﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlxApi.UmengPushSDK.Model
{
    /// <summary>
    /// 推送返回结果
    /// </summary>
    public class PushStatusResult
    {
        /// <summary>
        ///  "ret":"SUCCESS/FAIL", // 返回结果，"SUCCESS"或者"FAIL"
        /// </summary>
        public string ret { get; set; }
        public dataInfo data { get; set; }
        public class dataInfo
        {
            public string msg_id { get; set; }
            public string task_id { get; set; }
            /// <summary>
            /// 错误码详见附录I。
            /// </summary>
            public string error_code { get; set; }
            public string thirdparty_id { get; set; }

        }
    }
    public class PushStatus
    {
        /// <summary>
        /// 成功
        /// </summary>
        public static string SUCCESS = "SUCCESS";
        /// <summary>
        /// 失败
        /// </summary>
        public static string FAIL = "FAIL";
    }
}
