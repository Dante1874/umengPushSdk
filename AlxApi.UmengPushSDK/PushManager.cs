﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace AlxApi.UmengPushSDK
{
    /// <summary>
    /// 友盟推送。参考官方文档：http://dev.umeng.com/push/android/api-doc#1
    /// </summary>
    public class PushManager
    {

        #region IOS,Android 推送配置


        public static string APPKEY_IOS = System.Configuration.ConfigurationSettings.AppSettings["UmIosAppKey"];
        public static string APP_MASTER_SECRET_IOS = System.Configuration.ConfigurationSettings.AppSettings["UmIosAppMasterSecret"];
        public static string APPKEY_ANDROID = System.Configuration.ConfigurationSettings.AppSettings["UmAndroidAppKey"];
        public static string APP_MASTER_SECRET_ANDROID = System.Configuration.ConfigurationSettings.AppSettings["UmAndroidAppMasterSecret"];
        public static string APIURL = System.Configuration.ConfigurationSettings.AppSettings["UmPushApiUrl"];
        public static string PRODUCTION_MODE = System.Configuration.ConfigurationSettings.AppSettings["UmSdkDebug"];
        #endregion

        #region IOS


        /// <summary>
        /// IOS单播推送
        /// </summary>
        /// <param name="title">必填 标题</param>
        /// <param name="content">可选 建议填写</param>
        /// <param name="token">用户设备token</param>
        /// <returns></returns>
        public static Model.PushStatusResult PushIosUnicast(string title, string content, string token)
        {

            Model.IOSPushModel iosPushModel = new Model.IOSPushModel()
            {
                appkey = APPKEY_IOS,
                timestamp = PushBase.GetTimeStamp(),
                type = Model.PushType.Unicast,
                device_tokens = token,
                payload = new Model.IOSPushModel.payloadInfo()
                {
                    aps = new Model.IOSPushModel.payloadInfo.apsInfo()
                    {
                        alert = title,
                        content_available = content
                    }
                },
                description = content,
                production_mode = PRODUCTION_MODE
            };
            var result = PushBase.PushIosRequest(APIURL, iosPushModel, APP_MASTER_SECRET_IOS);

            return result;
        }

        /// <summary>
        /// Ios广播推送
        /// </summary>
        /// <param name="title">必填 标题</param>
        /// <param name="content">可选 建议填写</param>
        /// <param name="start_time">可选 定时发送时间，默认为立即发送。发送时间不能小于当前时间。</param>
        /// <param name="expire_time">可选 消息过期时间,其值不可小于发送时间,默认为3天后过期。格式同start_time</param>
        /// <returns></returns>
        public static Model.PushStatusResult PushIosBroadcast(string title, string content, string start_time, string expire_time)
        {

            Model.IOSPushModel pushModel = new Model.IOSPushModel()
            {
                appkey = APPKEY_IOS,
                timestamp = PushBase.GetTimeStamp(),
                type = Model.PushType.Broadcast,
                payload = new Model.IOSPushModel.payloadInfo()
                {
                    aps = new Model.IOSPushModel.payloadInfo.apsInfo()
                    {
                        alert = title,
                        content_available = content
                    }
                },

                policy = new Model.IOSPushModel.policyInfo()
                {
                    start_time = start_time,
                    expire_time = expire_time
                },
                description = content,
                production_mode = PRODUCTION_MODE
            };
            return PushBase.PushIosRequest(APIURL, pushModel, APP_MASTER_SECRET_IOS);
        }

        #endregion

        #region Android


        /// <summary>
        /// Android单播推送
        /// </summary>
        /// <param name="title">必填 标题</param>
        /// <param name="content">可选 建议填写</param>
        /// <param name="token">用户设备token</param>
        /// <returns></returns>
        public static Model.PushStatusResult PushAndroidUnicast(string title, string content, string token)
        {

            Model.AndroidPushModel androidPushModel = new Model.AndroidPushModel()
            {
                appkey = APPKEY_ANDROID,
                timestamp = PushBase.GetTimeStamp(),
                type = Model.PushType.Unicast,
                device_tokens = token,
                payload = new Model.AndroidPushModel.payloadInfo()
                {
                    display_type = "notification",
                    body = new Model.AndroidPushModel.payloadInfo.bodyInfo()
                    {
                        ticker = title,
                        title = title,
                        text = content,
                        after_open = "go_app",
                        activity = "",
                        builder_id = "0",
                        custom = "",
                        icon = "",
                        img = "",
                        largeIcon = "",
                        play_lights = "true",
                        play_sound = "true",
                        play_vibrate = "true",
                        sound = "",
                        url = ""
                    }
                },
                description = content,
                production_mode = PRODUCTION_MODE,
                alias = "",
                alias_type = "",
                file_id = "",
                filter = "",
                thirdparty_id = ""
            };
            return PushBase.PushAndroidRequest(APIURL, androidPushModel, APP_MASTER_SECRET_ANDROID);
        }

        /// <summary>
        /// Android广播推送
        /// </summary>
        /// <param name="title">必填 标题</param>
        /// <param name="content">可选 建议填写</param>
        /// <param name="start_time">可选 定时发送时间，默认为立即发送。发送时间不能小于当前时间。</param>
        /// <param name="expire_time">可选 消息过期时间,其值不可小于发送时间,默认为3天后过期。格式同start_time</param>
        /// <returns></returns>
        public static Model.PushStatusResult PushAndroidBroadcast(string title, string content, string start_time, string expire_time)
        {

            Model.AndroidPushModel androidPushModel = new Model.AndroidPushModel()
            {
                appkey = APPKEY_ANDROID,
                timestamp = PushBase.GetTimeStamp(),
                type = Model.PushType.Broadcast,
                device_tokens = "",
                payload = new Model.AndroidPushModel.payloadInfo()
                {
                    display_type = "notification",
                    body = new Model.AndroidPushModel.payloadInfo.bodyInfo()
                    {
                        ticker = title,
                        title = title,
                        text = content,
                        after_open = "go_app",
                        activity = "",
                        builder_id = "0",
                        custom = "",
                        icon = "",
                        img = "",
                        largeIcon = "",
                        play_lights = "true",
                        play_sound = "true",
                        play_vibrate = "true",
                        sound = "",
                        url = ""
                    }
                },
                description = content,
                production_mode = PRODUCTION_MODE,
                alias = "",
                alias_type = "",
                file_id = "",
                filter = "",
                thirdparty_id = ""
            };
            return PushBase.PushAndroidRequest(APIURL, androidPushModel, APP_MASTER_SECRET_ANDROID);
        }

        #endregion
    }
}
