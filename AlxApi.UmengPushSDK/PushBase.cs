﻿using AlxApi.UmengPushSDK.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AlxApi.UmengPushSDK
{

    /// <summary>
    /// 推送基类
    /// </summary>
    public class PushBase
    {
        /// <summary>
        /// 获取签名
        /// </summary>
        /// <param name="requestType">POST/GET</param>
        /// <param name="url">请求地址</param>
        /// <param name="pushModelJson">请求内容json</param>
        /// <param name="appMasterSecret">appMasterSecret密钥</param>
        /// <returns></returns>
        public static string GetMysign(string requestType, string url, string pushModelJson, string appMasterSecret)
        {
            return GetMD5Hash(requestType.ToUpper() + url + pushModelJson + appMasterSecret);
        }
        /// <summary>
        /// 计算MD5
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static String GetMD5Hash(String str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
            StringBuilder strReturn = new StringBuilder();

            for (int i = 0; i < result.Length; i++)
            {
                strReturn.Append(Convert.ToString(result[i], 16).PadLeft(2, '0'));
            }

            return strReturn.ToString().PadLeft(32, '0');

        }
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// IOS推送
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="model">IOSPushModel</param>
        /// <param name="appMasterSecret">appMasterSecret</param>
        /// <returns></returns>
        public static Model.PushStatusResult PushIosRequest(string url, Model.IOSPushModel model, string appMasterSecret)
        {
            Model.PushStatusResult result = new Model.PushStatusResult();
            try
            {
                model.payload.aps.sound = "Mr1";
                var modelJson = JsonHelper.GetJson(model);
                //content-available
                modelJson = modelJson.Replace("content_available", "content-available");
                var sign = GetMysign("POST", url, modelJson, appMasterSecret);
                string requestUrl = url + "?sign=" + sign;
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.Method = "POST";
                byte[] bs = Encoding.UTF8.GetBytes(modelJson);
                request.ContentLength = bs.Length;
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                    reqStream.Close();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string resultStr = reader.ReadToEnd();
                result = JsonHelper.ParseFromJson<Model.PushStatusResult>(resultStr);
                HttpStatusCode statusCode = response.StatusCode;

            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    Stream myResponseStream = ((HttpWebResponse)e.Response).GetResponseStream();
                    StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                    string retString = myStreamReader.ReadToEnd();
                    result = JsonHelper.ParseFromJson<Model.PushStatusResult>(retString);
                }
            }
            return result;

        }
        /// <summary>
        /// Android推送
        /// </summary>
        /// <param name="url"></param>
        /// <param name="model"></param>
        /// <param name="appMasterSecret"></param>
        /// <returns></returns>
        public static Model.PushStatusResult PushAndroidRequest(string url, Model.AndroidPushModel model, string appMasterSecret)
        {
            Model.PushStatusResult result = new Model.PushStatusResult();
            try
            {
                var modelJson = JsonHelper.GetJson(model);
                var sign = GetMysign("POST", url, modelJson, appMasterSecret);
                string requestUrl = url + "?sign=" + sign;
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.Method = "POST";
                byte[] bs = Encoding.UTF8.GetBytes(modelJson);
                request.ContentLength = bs.Length;
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                    reqStream.Close();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string resultStr = reader.ReadToEnd();
                result = JsonHelper.ParseFromJson<Model.PushStatusResult>(resultStr);
                HttpStatusCode statusCode = response.StatusCode;

            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    Stream myResponseStream = ((HttpWebResponse)e.Response).GetResponseStream();
                    StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                    string retString = myStreamReader.ReadToEnd();
                    result = JsonHelper.ParseFromJson<Model.PushStatusResult>(retString);
                }
            }
            return result;
        }
    }
}